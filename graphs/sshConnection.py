import os
import subprocess
import sys

import ipaddress
import paramiko


def is_ip_address(ip_address):
    try:
        ipaddress.ip_address(ip_address)
        return True
    except ValueError as e:
        print(str(e))
        return False


def connect(host, user_name, private_rsa_key_path, host_keys_path):
    client = paramiko.SSHClient()
    # if you don't have a host key file, you can create an empty file and let paramiko populate it for you
    client.load_host_keys(host_keys_path)
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print("ssh -i " + private_rsa_key_path + " " + user_name + "@" + host)
    print("Using the known_hosts file " + host_keys_path)
    client.connect(host, username=user_name, key_filename=private_rsa_key_path)
    if not client.get_transport().is_active():
        print("SSH session failed on login.")
    else:
        print("SSH session login successful")
        return client


def ignored(path, git_root):

    pwd = os.curdir

    try:
        os.chdir(git_root)
        subprocess.check_call(["git", "check-ignore", path], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
        return True
    except subprocess.CalledProcessError as e:
        if e.returncode == 1:
            return path.find(".git") >= 0
        else:
            return True  # If not in the git repo
    finally:
        os.chdir(pwd)


def sync_directory(client, local_path, remote_path, git_root="."):

    sftp_client = client.open_sftp()

    print(local_path)

    for root, dirs, files in os.walk(local_path):

        current_sub_path = str(root).replace(local_path, "")  # Remove the local part of the directory name
        if len(current_sub_path) != 0:  # not the root
            current_sub_path = current_sub_path[1:]  # Remove the first slash
            current_remote_path = os.path.join(remote_path, current_sub_path)
        else:
            current_remote_path = remote_path

        if not ignored(root, git_root):

            print('\t  -> Copying ', root, ' to ', current_remote_path)

            # Create the current directory
            try:
                sftp_client.chdir(current_remote_path)  # Test if remote_path exists
            except IOError:
                sftp_client.mkdir(current_remote_path)  # Create remote_path

            # Copy files
            for file_name in files:

                local_file_path = os.path.join(root, file_name)
                remote_file_path = os.path.join(current_remote_path, file_name)

                if not ignored(local_file_path, git_root):  # If this file is not ignored

                    print('\t  -> Copying ', local_file_path, ' to ', remote_file_path)
                    sftp_client.put(local_file_path, remote_file_path)
                    sftp_client.chmod(path=remote_file_path, mode=os.stat(local_file_path).st_mode)

    sftp_client.close()


def get_dir_list(client, remote_path):

    sftp_client = client.open_sftp()
    files_and_subdirectories = sftp_client.listdir_attr(remote_path)
    sftp_client.close()
    return files_and_subdirectories


def sync_file(client, local_path, remote_path):

    sftp_client = client.open_sftp()

    print('\t  -> Copying ', local_path, ' to ', remote_path)

    sftp_client.put(local_path, remote_path)
    sftp_client.chmod(path=remote_path, mode=os.stat(local_path).st_mode)

    sftp_client.close()


def download_file(client, remote_path, local_path):

    sftp_client = client.open_sftp()

    print('\t  -> Copying ', remote_path, ' to ', local_path)

    sftp_client.get(remote_path, local_path)

    sftp_client.close()


def send_command(client, command, silent=False, output_file=None, error_file=None):

    print("Command : \"" + command + "\"")
    stdin, stdout, stderr = client.exec_command(command)

    output_file_obj = None
    error_file_obj = None
    if output_file is not None:
        output_file_obj = open(output_file, "w")
        output_file_obj.write("Command : \"" + command + "\"\n")
    if error_file is not None:
        error_file_obj = open(output_file, "w")
        error_file_obj.write("Command : \"" + command + "\"\n")

    for line in stdout:
        if not silent:
            sys.stdout.write(line)
        elif output_file_obj is not None:
            output_file_obj.write(line)

    for line in stderr:
        if not silent:
            sys.stderr.write(line)
        elif error_file_obj is not None:
            error_file_obj.write(line)

    stdin.close()
    stdout.close()
    stderr.close()
    if output_file_obj is not None:
        output_file_obj.close()
    if error_file_obj is not None:
        error_file_obj.close()


def logout(client):
    client.close()
