#!/usr/bin/env python3

import sys
import os
import time
import shutil
import stat

from sshConnection import is_ip_address, connect, logout, download_file, get_dir_list
from data_handlers import handlers

SRC_DIRECTORY = os.path.join(os.path.dirname(os.path.dirname(sys.argv[0])))
CLIENT_DIRECTORY = "client"
SERVER_DIRECTORY = "server"
GRAPH_DIRECTORY = "graphs"


def copy_files_with_prefix(ssh_client, src_directory, prefix, dst_directory):

    if ssh_client is None:  # Local directory
        for (dir_path, dir_names, file_names) in os.walk(src_directory):
            for file_name in file_names:
                if os.path.basename(file_name).find(prefix) >= 0:
                    local_path = os.path.join(dst_directory, os.path.basename(file_name))
                    os.makedirs(os.path.dirname(local_path), mode=0o777, exist_ok=True)
                    shutil.copy(src=os.path.join(src_directory, file_name), dst=local_path)
            break
    else:  # Remote directory
        for file_stat in get_dir_list(client, src_directory):
            if not stat.S_ISDIR(file_stat.st_mode) and file_stat.filename.find(prefix) >= 0:
                local_path = os.path.join(dst_directory, file_stat.filename)
                os.makedirs(os.path.dirname(local_path), mode=0o777, exist_ok=True)
                download_file(client=client, remote_path=os.path.join(src_directory, file_stat.filename),
                              local_path=local_path)


def show_help():
    print("Usage :")
    print("Usage : ./graph.py localBinDirectory remoteBinDirectory remoteUser remoteIP rsaKeyFile hostKeyPath"
          " isClientLocal [produceGraphOnly]\n")
    print("isClientLocal = True|False whether the client was the localhost or not")
    print("To print this message : ./graph.py -h\n")

# Arguments parsing
if len(sys.argv) == 1 or sys.argv[1] == "-h" or len(sys.argv) < 8:
    show_help()
    sys.exit(1)

localBinDirectory = sys.argv[1]
if not os.path.isdir(localBinDirectory):
    print("ERROR : Local bin directory " + sys.argv[1] + " doesn't exist !")
    show_help()
    sys.exit(1)

remoteBinDirectory = sys.argv[2]
remoteUser = sys.argv[3]

if not is_ip_address(sys.argv[4]):
    print("ERROR : Bad IP address !")
    show_help()
    sys.exit(1)
ip_address = sys.argv[4]

rsaKeyFile = sys.argv[5]

if not os.path.isfile(sys.argv[6]):
    print("ERROR : host keys file " + sys.argv[6] + " doesn't exist !")
    show_help()
    sys.exit(1)
host_keys_path = sys.argv[6]

isClientLocal = sys.argv[7] == "True"

produceGraphOnly = len(sys.argv) >= 9 and sys.argv[8] == "True"


start_execution = time.time()

# Start SSH connection
client = None
if not produceGraphOnly:
    client = connect(ip_address, remoteUser, rsaKeyFile, host_keys_path)
    if client is None:
        sys.exit(1)

for handler in handlers:

    print("We are creating graphs for performance test : " + handler.method)

    if client is not None:

        clientBinDirectory = localBinDirectory if isClientLocal else remoteBinDirectory
        serverBinDirectory = localBinDirectory if not isClientLocal else remoteBinDirectory

        for client_key, client_data_file_prefix in handler.client_data_files.items():
            src_client_data_directory = os.path.join(clientBinDirectory, CLIENT_DIRECTORY)
            dst_client_data_directory = os.path.join(SRC_DIRECTORY, GRAPH_DIRECTORY, handler.graph_sub_folder)
            copy_files_with_prefix(None if isClientLocal else client, src_client_data_directory,
                                   client_data_file_prefix, dst_client_data_directory)

        for server_key, server_data_file_prefix in handler.server_data_files.items():
            src_server_data_directory = os.path.join(serverBinDirectory, SERVER_DIRECTORY)
            dst_server_data_directory = os.path.join(SRC_DIRECTORY, GRAPH_DIRECTORY, handler.graph_sub_folder)
            copy_files_with_prefix(None if not isClientLocal else client, src_server_data_directory,
                                   server_data_file_prefix, dst_server_data_directory)

    handler.handle(sys.argv[0])


# Close the SSH connection
if client is not None:
    logout(client)

execution_time = int(time.time() - start_execution)

print("\nThe whole script took " + str(execution_time) + " seconds.")
