import os
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod

from scapy.all import *


TLS = "tls"
MPTCPSEC = "mptcpsec"
MPTCP = "mptcp"
RESULTS_SUB_FOLDER = "results"


class DataHandler(ABC):

    def __init__(self, server_data_files, client_data_files, graph_sub_folder, method):

        self.server_data_files = server_data_files
        self.client_data_files = client_data_files
        self.graph_sub_folder = graph_sub_folder
        self.method = method

    @staticmethod
    def convert_bytes_to_correct_unit(bytes_amount):

        bytes_amount = int(bytes_amount)
        if bytes_amount % (10**9) == 0:
            return str(int(bytes_amount / (10**9))) + "GB"
        elif bytes_amount % (10**6) == 0:
            return str(int(bytes_amount / (10**6))) + "MB"
        elif bytes_amount % (10**3) == 0:
            return str(int(bytes_amount / (10**3))) + "kB"
        else:
            return str(bytes_amount) + "B"

    def __str__(self):
        return self.method

    @abstractmethod
    def handle(self, script_path):
        pass


class ThroughputMeasureHandler(DataHandler):

    def __init__(self):

        super(ThroughputMeasureHandler, self).__init__(
            server_data_files={TLS: "tls_throughput_measure_server", MPTCPSEC: "mptcpsec_throughput_measure_server",
                               MPTCP: "mptcp_throughput_measure_server"},
            client_data_files={TLS: "tls_throughput_measure_client", MPTCPSEC: "mptcpsec_throughput_measure_client",
                               MPTCP: "mptcp_throughput_measure_client"},
            graph_sub_folder="throughput_measure",
            method="Throughput measure")

    @staticmethod
    def convert_to_microseconds(number_str):

        seconds = number_str[:number_str.find(".")]
        if len(number_str) > len(seconds):
            microseconds = number_str[number_str.find(".")+1:]
        else:
            microseconds = "0"

        return float(int(seconds) * 10**6 + int(microseconds))

    @staticmethod
    def interpret_file(times, file):

        start_time = -1.0
        with open(file, "r") as file_obj:
            for line in file_obj:
                if start_time < 0 and len(line) > 1:
                    start_time = (ThroughputMeasureHandler.convert_to_microseconds(line[:-1]))
                if len(line) > 1:
                    times.append((ThroughputMeasureHandler.convert_to_microseconds(line[:-1]) - start_time) / 1000.0)

    @staticmethod
    def compute_troughput(bytes_transferred, graph_directory, list_of_files):

        total_time = 0.0

        for file_name in list_of_files:
            start_time_point = -1
            duration = 0
            with open(os.path.join(graph_directory, file_name), "r") as file_obj:
                previous_duration = 0.0
                for line in file_obj:
                    previous_duration = duration
                    if start_time_point < 0:
                        start_time_point = (ThroughputMeasureHandler.convert_to_microseconds(line[:-1]))
                    else:
                        duration = (ThroughputMeasureHandler.convert_to_microseconds(line[:-1]) - start_time_point) / \
                                   1000.0
                total_time += previous_duration

        return (bytes_transferred * len(list_of_files) * 1000.0) / total_time

    def handle(self, script_path):

        graph_directory = os.path.join(os.path.dirname(script_path), self.graph_sub_folder)

        tls_client_file_prefix = self.client_data_files[TLS]
        tls_server_file_prefix = self.server_data_files[TLS]
        mptcpsec_client_file_prefix = self.client_data_files[MPTCPSEC]
        mptcpsec_server_file_prefix = self.server_data_files[MPTCPSEC]
        mptcp_client_file_prefix = self.client_data_files[MPTCP]
        mptcp_server_file_prefix = self.server_data_files[MPTCP]
        tls_client_dict = {}
        tls_server_dict = {}
        mptcpsec_client_dict = {}
        mptcpsec_server_dict = {}
        mptcp_client_dict = {}
        mptcp_server_dict = {}

        for (dir_path, dir_names, file_names) in os.walk(graph_directory):
            for file_name in file_names:

                # Find the number of bytes transferred in this file
                bytes_transferred_index = os.path.basename(file_name).rfind("_") + 1
                if bytes_transferred_index == 0 or bytes_transferred_index >= len(os.path.basename(file_name)):
                    continue
                bytes_transferred = os.path.basename(file_name)[bytes_transferred_index:]
                try:
                    bytes_transferred = int(bytes_transferred)
                except ValueError:
                    continue

                # Record it in one of the dictionaries
                if os.path.basename(file_name).find(tls_client_file_prefix) >= 0:
                    if bytes_transferred not in tls_client_dict:
                        tls_client_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    else:
                        tls_client_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                elif os.path.basename(file_name).find(tls_server_file_prefix) >= 0:
                    if bytes_transferred not in tls_server_dict:
                        tls_server_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    else:
                        tls_server_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                elif os.path.basename(file_name).find(mptcpsec_client_file_prefix) >= 0:
                    if bytes_transferred not in mptcpsec_client_dict:
                        mptcpsec_client_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    else:
                        mptcpsec_client_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                elif os.path.basename(file_name).find(mptcpsec_server_file_prefix) >= 0:
                    if bytes_transferred not in mptcpsec_server_dict:
                        mptcpsec_server_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    else:
                        mptcpsec_server_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                elif os.path.basename(file_name).find(mptcp_client_file_prefix) >= 0:
                    if bytes_transferred not in mptcp_client_dict:
                        mptcp_client_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    else:
                        mptcp_client_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                elif os.path.basename(file_name).find(mptcp_server_file_prefix) >= 0:
                    if bytes_transferred not in mptcp_server_dict:
                        mptcp_server_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    else:
                        mptcp_server_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))

            break  # We don't want recursive search inside the directory

        tls_client_throughput_list_temp = []
        mptcpsec_client_throughput_list_temp = []
        mptcp_client_throughput_list_temp = []
        tls_server_throughput_list_temp = []
        mptcpsec_server_throughput_list_temp = []
        mptcp_server_throughput_list_temp = []

        # Build comparison graph between TLS and MPTCPsec for each different transfer size
        for key, value in mptcpsec_client_dict.items():

            if key in tls_client_dict and key in tls_server_dict and key in mptcpsec_server_dict and \
                            key in mptcp_server_dict and key in mptcp_client_dict:

                # Interpret data
                tls_client_times = []
                tls_server_times = []
                mptcpsec_client_times = []
                mptcpsec_server_times = []
                mptcp_client_times = []
                mptcp_server_times = []
                self.interpret_file(tls_client_times, os.path.join(graph_directory, tls_client_dict[key][0]))
                self.interpret_file(tls_server_times, os.path.join(graph_directory, tls_server_dict[key][0]))
                self.interpret_file(mptcpsec_client_times, os.path.join(graph_directory, mptcpsec_client_dict[key][0]))
                self.interpret_file(mptcpsec_server_times, os.path.join(graph_directory, mptcpsec_server_dict[key][0]))
                self.interpret_file(mptcp_client_times, os.path.join(graph_directory, mptcp_client_dict[key][0]))
                self.interpret_file(mptcp_server_times, os.path.join(graph_directory, mptcp_server_dict[key][0]))

                if key >= 50000000:
                    tls_client_throughput_list_temp.append((key, self.compute_troughput(key, graph_directory,
                                                                                        tls_client_dict[key])))
                    mptcpsec_client_throughput_list_temp.append((key, self.compute_troughput(key, graph_directory,
                                                                                             mptcpsec_client_dict[
                                                                                                 key])))
                    mptcp_client_throughput_list_temp.append((key, self.compute_troughput(key, graph_directory,
                                                                                          mptcp_client_dict[key])))
                    tls_server_throughput_list_temp.append((key, self.compute_troughput(key, graph_directory,
                                                                                        tls_server_dict[key])))
                    mptcpsec_server_throughput_list_temp.append((key, self.compute_troughput(key, graph_directory,
                                                                                             mptcpsec_server_dict[
                                                                                                 key])))
                    mptcp_server_throughput_list_temp.append((key, self.compute_troughput(key, graph_directory,
                                                                                          mptcp_server_dict[key])))

                # Build graphs
                graph_client_comparison_title = "Transferred data from client perspective (transfer size = " + \
                                                self.convert_bytes_to_correct_unit(key) + ")\n"
                graph_client_comparison_file_name = "client_throughput_measure_" + str(key) + ".svg"
                graph_server_comparison_title = "Transferred Data from server perspective (transfer size = " + \
                                                self.convert_bytes_to_correct_unit(key) + ")\n"
                graph_server_comparison_file_name = "server_throughput_measure_" + str(key) + ".svg"

                self.plot_tls_vs_mptcpsec_comparison(tls_client_times, mptcpsec_client_times, mptcp_client_times,
                                                     title=graph_client_comparison_title,
                                                     graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                 graph_client_comparison_file_name))
                self.plot_tls_vs_mptcpsec_comparison(tls_server_times, mptcpsec_server_times, mptcp_server_times,
                                                     title=graph_server_comparison_title,
                                                     graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                 graph_server_comparison_file_name))

        # Parse data in order to obtain the throughput

        tls_client_throughput_list_temp = sorted(tls_client_throughput_list_temp, key=lambda x: x[0])
        mptcpsec_client_throughput_list_temp = sorted(mptcpsec_client_throughput_list_temp, key=lambda x: x[0])
        mptcp_client_throughput_list_temp = sorted(mptcp_client_throughput_list_temp, key=lambda x: x[0])
        tls_server_throughput_list_temp = sorted(tls_server_throughput_list_temp, key=lambda x: x[0])
        mptcpsec_server_throughput_list_temp = sorted(mptcpsec_server_throughput_list_temp, key=lambda x: x[0])
        mptcp_server_throughput_list_temp = sorted(mptcp_server_throughput_list_temp, key=lambda x: x[0])

        data_transferred = [pair[0] / 10**6 for pair in tls_client_throughput_list_temp]
        tls_client_throughput_list = [pair[1] * 8 / 10**6 for pair in tls_client_throughput_list_temp]
        mptcpsec_client_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcpsec_client_throughput_list_temp]
        mptcp_client_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcp_client_throughput_list_temp]
        tls_server_throughput_list = [pair[1] * 8 / 10**6 for pair in tls_server_throughput_list_temp]
        mptcpsec_server_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcpsec_server_throughput_list_temp]
        mptcp_server_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcp_server_throughput_list_temp]

        # Build throughput graph (w.r.t. the data transfer size)
        graph_client_comparison_title = "Throughput from client perspective\n"
        graph_client_comparison_file_name = "client_throughput_comparison.svg"
        graph_server_comparison_title = "Throughput from server perspective\n"
        graph_server_comparison_file_name = "server_throughput_comparison.svg"

        self.plot_throughput_tls_vs_mptcpsec_comparison(data_transferred, tls_client_throughput_list,
                                                        mptcpsec_client_throughput_list, mptcp_client_throughput_list,
                                                        title=graph_client_comparison_title,
                                                        graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                    graph_client_comparison_file_name))

        self.plot_throughput_tls_vs_mptcpsec_comparison(data_transferred, tls_server_throughput_list,
                                                        mptcpsec_server_throughput_list, mptcp_server_throughput_list,
                                                        title=graph_server_comparison_title,
                                                        graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                    graph_server_comparison_file_name))

    @staticmethod
    def plot_throughput_tls_vs_mptcpsec_comparison(data_transferred, tls_times, mptcpsec_times, mptcp_times, title,
                                                   graph_dst_file):

        # new frame
        fig = plt.figure()

        # plot data
        if len(data_transferred) < 200:
            plt.plot(data_transferred, tls_times, 'ko', label="TLS", color='blue')
            plt.plot(data_transferred, mptcpsec_times, 'ko', label="MPTCPsec", color='green')
            plt.plot(data_transferred, mptcp_times, 'ko', label="MPTCP", color='red')
        else:
            plt.plot(data_transferred, tls_times, label="TLS")
            plt.plot(data_transferred, mptcpsec_times, label="MPTCPsec")
            plt.plot(data_transferred, mptcp_times, label="MPTCP")

        # add in labels and title
        plt.xlabel("Data transferred (MB)")
        plt.ylabel("Throughput (Mb/s)")
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # Dezoom the graph
        plt.ylim(75, 100)

        # save figure
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)

    @staticmethod
    def plot_tls_vs_mptcpsec_comparison(tls_times, mptcpsec_times, mptcp_times, title, graph_dst_file):

        data_transferred = [y*4096 for y in range(len(mptcpsec_times))]

        # new frame
        fig = plt.figure()

        # plot data
        if len(data_transferred) < 200:
            plt.plot(tls_times, data_transferred, 'ko', label="TLS", color='blue')
            plt.plot(mptcpsec_times, data_transferred, 'ko', label="MPTCPsec", color='green')
            plt.plot(mptcp_times, data_transferred, 'ko', label="MPTCP", color='red')
        else:
            plt.plot(tls_times, data_transferred, label="TLS")
            plt.plot(mptcpsec_times, data_transferred, label="MPTCPsec")
            plt.plot(mptcp_times, data_transferred, label="MPTCP")

        # add in labels and title
        plt.xlabel("Time (ms) starting from the end of the connection establishment")
        plt.ylabel("Transferred Data (bytes)")
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # save figure
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)


class ProcessingMeasureHandler(DataHandler):

    def __init__(self):
        super(ProcessingMeasureHandler, self).__init__(
            server_data_files={TLS: "tls_processing_measure_server",
                               MPTCPSEC: "mptcpsec_processing_measure_server",
                               MPTCP: "mptcp_processing_measure_server"},
            client_data_files={TLS: "tls_processing_measure_server",
                               MPTCPSEC: "mptcpsec_processing_measure_server",
                               MPTCP: "mptcp_processing_measure_server"},
            graph_sub_folder="processing_measure",
            method="Processing measure")

    @staticmethod
    @abstractmethod
    def interpret_cap_files(protocol, sent_size, graph_directory, files):
        pass

    @staticmethod
    @abstractmethod
    def find_byte_transferred(file_name):
        pass

    @staticmethod
    @abstractmethod
    def plot_tls_vs_mptcpsec_comparison(sent_sizes, tls_durations, mptcpsec_durations, mptcp_durations, title,
                                        graph_dst_file):
        pass

    def handle(self, script_path):

        graph_directory = os.path.join(os.path.dirname(script_path), self.graph_sub_folder)

        tls_server_file_prefix = self.server_data_files[TLS]
        mptcpsec_server_file_prefix = self.server_data_files[MPTCPSEC]
        mptcp_server_file_prefix = self.server_data_files[MPTCP]
        tls_server_dict = {}
        mptcpsec_server_dict = {}
        mptcp_server_dict = {}

        for (dir_path, dir_names, file_names) in os.walk(graph_directory):
            for file_name in file_names:

                if os.path.basename(file_name).find(".pcap") >= 0:  # Only captures

                    # Find the number of bytes transferred in this file
                    bytes_transferred = self.find_byte_transferred(file_name)
                    if bytes_transferred < 0:
                        continue

                    # Record it in one of the dictionaries
                    if os.path.basename(file_name).find(tls_server_file_prefix) >= 0:
                        if bytes_transferred in tls_server_dict:
                            tls_server_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                        else:
                            tls_server_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    elif os.path.basename(file_name).find(mptcpsec_server_file_prefix) >= 0:
                        if bytes_transferred in mptcpsec_server_dict:
                            mptcpsec_server_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                        else:
                            mptcpsec_server_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]
                    elif os.path.basename(file_name).find(mptcp_server_file_prefix) >= 0:
                        if bytes_transferred in mptcp_server_dict:
                            mptcp_server_dict[bytes_transferred].append(os.path.join(graph_directory, file_name))
                        else:
                            mptcp_server_dict[bytes_transferred] = [os.path.join(graph_directory, file_name)]

            break  # We don't want recursive search inside the directory

        # Interpret data
        tls_server_pairs = []
        mptcpsec_server_pairs = []
        mptcp_server_pairs = []

        for key, value in mptcpsec_server_dict.items():

            if key in tls_server_dict and key in mptcp_server_dict:
                tls_server_pairs.append((key, self.interpret_cap_files(TLS, key, graph_directory,
                                                                       tls_server_dict[key])))
                mptcpsec_server_pairs.append((key, self.interpret_cap_files(MPTCPSEC, key, graph_directory,
                                                                            mptcpsec_server_dict[key])))
                mptcp_server_pairs.append((key, self.interpret_cap_files(MPTCP, key, graph_directory,
                                                                         mptcp_server_dict[key])))

        # Order measures
        tls_server_pairs = sorted(tls_server_pairs, key=lambda x: x[0])
        print("TLS = " + str(tls_server_pairs))
        mptcpsec_server_pairs = sorted(mptcpsec_server_pairs, key=lambda x: x[0])
        print("MPTCPsec = " + str(mptcpsec_server_pairs))
        mptcp_server_pairs = sorted(mptcp_server_pairs, key=lambda x: x[0])
        print("MPTCP = " + str(mptcp_server_pairs))

        sent_sizes = [pair[0] for pair in mptcpsec_server_pairs]
        tls_server_durations = [pair[1] for pair in tls_server_pairs]
        mptcpsec_server_durations = [pair[1] for pair in mptcpsec_server_pairs]
        mptcp_server_durations = [pair[1] for pair in mptcp_server_pairs]

        # Build comparison graph between TLS and MPTCPsec for each different transfer size
        graph_server_comparison_title = "Echo and reply of different sizes\n"
        graph_server_comparison_file_name = "pingpong_server_comparison.svg"

        self.plot_tls_vs_mptcpsec_comparison(sent_sizes, tls_server_durations, mptcpsec_server_durations,
                                             mptcp_server_durations, title=graph_server_comparison_title,
                                             graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                         graph_server_comparison_file_name))


class SegmentProcessingMeasureHandler(ProcessingMeasureHandler):

    def __init__(self):

        super(SegmentProcessingMeasureHandler, self).__init__()

    @staticmethod
    def interpret_cap_files(protocol, sent_size, graph_directory, files):

        mean_ping_pong_time = 0.0
        iterations = 0.0

        cipher_size = sent_size
        if protocol == MPTCPSEC:
            cipher_size = sent_size + 3 * 16  # Option tag + data tag
        elif protocol == TLS:
            cipher_size = sent_size + 5 + 8 + 16  # (content type, version and length) + explicit nonce + tag

        for file_name in files:
            ping_time = -1.0
            pong_time = -1.0
            packet_list = rdpcap(os.path.join(graph_directory, file_name))

            for packet in packet_list:

                if TCP in packet and len(packet[TCP].payload) == cipher_size:
                    if ping_time < 0:
                        ping_time = packet.time
                    else:
                        pong_time = packet.time
                        break  # Both measures were found => we can leave the loop

            if ping_time > 0.0 and pong_time > 0.0:
                mean_ping_pong_time += ((pong_time - ping_time) * 1000.0)
                iterations += 1

        return -1 if iterations == 0.0 else mean_ping_pong_time / iterations

    @staticmethod
    def find_byte_transferred(file_name):

        bytes_transferred_index = os.path.basename(file_name).rfind("_") + 1
        if bytes_transferred_index == 0 or bytes_transferred_index >= len(os.path.basename(file_name)):
            return -1
        bytes_transferred = os.path.basename(file_name)[bytes_transferred_index:-5]
        try:
            bytes_transferred = int(bytes_transferred)
        except ValueError:
            return -1

        return bytes_transferred

    @staticmethod
    def plot_tls_vs_mptcpsec_comparison(sent_sizes, tls_durations, mptcpsec_durations, mptcp_durations, title,
                                        graph_dst_file):

        # new frame
        fig = plt.figure()

        # plot data
        if len(sent_sizes) < 200:
            plt.plot(sent_sizes, tls_durations, 'ko', label="TLS", color='blue')
            plt.plot(sent_sizes, mptcpsec_durations, 'ko', label="MPTCPsec", color='green')
            plt.plot(sent_sizes, mptcp_durations, 'ko', label="MPTCP", color='red')
        else:
            plt.plot(sent_sizes, tls_durations, label="TLS")
            plt.plot(sent_sizes, mptcpsec_durations, label="MPTCPsec")
            plt.plot(sent_sizes, mptcp_durations, label="MPTCP")

        # add in labels and title
        plt.xlabel("Message data size (bytes)")
        plt.ylabel("Time (ms) between ping reception and pong sending")  # TODO Put correct scale !!!
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # save figure
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)


class WindowProcessingMeasureHandler(ProcessingMeasureHandler):

    def __init__(self):

        super(WindowProcessingMeasureHandler, self).__init__()

    @staticmethod
    def interpret_cap_files(protocol, sent_size, graph_directory, files):

        protocol_close_data_size = 0
        if protocol == TLS:
            # (content type, version and length) + explicit nonce + tag + alert
            protocol_close_data_size = 5 + 8 + 16 + 2

        ping_pong_time_list = []
        for file_name in files:
            ping_time = -1
            pong_time = -1
            last_pong_time = None
            packet_list = rdpcap(os.path.join(graph_directory, file_name))

            for packet in packet_list:

                if TCP in packet and len(packet[TCP].payload) > 0:
                    if ping_time < 0:
                        ping_time = packet.time
                        pong_src_address = packet[IP].dst
                    elif protocol_close_data_size != 0 and len(packet[TCP].payload) == protocol_close_data_size:
                        pong_time = packet.time
                        break
                    elif pong_src_address is not None and pong_src_address == packet[IP].src:
                        last_pong_time = packet.time

            if last_pong_time is not None:
                pong_time = last_pong_time

            if ping_time > 0 and pong_time > 0:
                ping_pong_time_list.append((pong_time - ping_time) * 1000)

        if len(ping_pong_time_list) == 0:
            return -1

        ping_pong_time_list = sorted(ping_pong_time_list)

        if len(ping_pong_time_list) % 2 != 0:
            median = ping_pong_time_list[len(ping_pong_time_list) // 2]
        else:
            median = float(ping_pong_time_list[len(ping_pong_time_list) // 2] +
                           ping_pong_time_list[(len(ping_pong_time_list) // 2) + 1]) / 2.0
        return median

    @staticmethod
    def find_byte_transferred(file_name):

        if file_name.find("window") < 0:
            return -1

        bytes_transferred_index_last = os.path.basename(file_name).rfind("_")
        if bytes_transferred_index_last == 0 or bytes_transferred_index_last >= len(os.path.basename(file_name)):
            return -1
        bytes_transferred_index = os.path.basename(file_name).rfind("_", 0, bytes_transferred_index_last) + 1
        bytes_transferred = os.path.basename(file_name)[bytes_transferred_index:bytes_transferred_index_last]
        try:
            bytes_transferred = int(bytes_transferred)
        except ValueError:
            return -1

        return bytes_transferred

    @staticmethod
    def plot_tls_vs_mptcpsec_comparison(sent_sizes, tls_durations, mptcpsec_durations, mptcp_durations, title,
                                        graph_dst_file):

        # new frame
        fig = plt.figure()

        # plot data
        if len(sent_sizes) < 200:
            plt.plot(sent_sizes, tls_durations, 'ko', label="TLS", color='blue')
            plt.plot(sent_sizes, mptcpsec_durations, 'ko', label="MPTCPsec", color='green')
            plt.plot(sent_sizes, mptcp_durations, 'ko', label="MPTCP", color='red')
        else:
            plt.plot(sent_sizes, tls_durations, label="TLS")
            plt.plot(sent_sizes, mptcpsec_durations, label="MPTCPsec")
            plt.plot(sent_sizes, mptcp_durations, label="MPTCP")

        # add in labels and title
        plt.xlabel("Message data size (bytes)")
        plt.ylabel("Time (ms) between ping reception and pong sending")  # TODO Put correct scale !!!
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # save figure
        graph_dst_base_name = os.path.basename(graph_dst_file)
        graph_dst_base_name = (str(graph_dst_base_name.split(".")[0]) + "_window." +
                               str(graph_dst_base_name.split(".")[1]))
        graph_dst_file = os.path.join(os.path.dirname(graph_dst_file), graph_dst_base_name)
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)


handlers = [ThroughputMeasureHandler(), SegmentProcessingMeasureHandler(), WindowProcessingMeasureHandler()]
