#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>

#include "../include/main.h"
#include "../include/throughput_measure.h"
#include "../include/pingpong.h"

void help() {

    fflush(stderr);
    fflush(stdout);
    printf("Usage : ./client serverIP serverPort protocol behaviour\n");
    printf("\tprotocol = tls|mptcpsec|mptcp\n");
    printf("\tbehaviour = [throughput_measure bytesToExchange [iteration_step repeat_number]]|"
                   "[pingpong iterations [noCapture]]|[max_connections bytesToExchange]|"
                   "[pingpong_window iterations [noCapture]]\n");
    printf("\tbytesToExchange = number of bytes to transfer in one connection\n");
    printf("\titerations = number of times that the measures should be rerun\n");
    printf("\tnoCapture = 0|1 whether there is a capture on the server side or not\n");
    printf("To print this message : ./client -h\n");
}

int main(int argc, char *argv []) {

    int opt = 0;
    int protocol;
    long bytesToExchange = 0;
    long iterations = 0;
    char *serverIP = NULL;
    char *ptr = NULL;
    char *serverPort = 0;
    int err = 0;

    while ((opt = getopt(argc, argv, "h")) != -1) {
        switch (opt) {
            case 'h':
                help();
                exit(EXIT_SUCCESS);
            default: /* '?' */
                help();
                exit(EXIT_FAILURE);
        }
    }

    if (argc < 6) {
        errno = EINVAL;
        perror("Invalid number of arguments ");
        help();
        exit(EXIT_FAILURE);
    }

    serverIP = argv[1];
    serverPort = argv[2];
    strtol(serverPort, &ptr, 10);
    if (*ptr != '\0') {
        errno = EINVAL;
        perror("Not a valid port number :\n");
        help();
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[3], "mptcpsec")) {
        protocol = MPTCPSEC;
    } else if (!strcmp(argv[3], "tls")) {
        protocol = TLS;
        SSL_library_init();
        SSL_load_error_strings();
        ERR_load_BIO_strings();
        OpenSSL_add_all_algorithms();
    } else if (!strcmp(argv[3], "mptcp")) {
        protocol = MPTCP;
    } else {
        errno = EINVAL;
        perror("Not a valid protocol :\n");
        help();
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[4], "throughput_measure")) {
        bytesToExchange = strtoll(argv[5], &ptr, 10);
        if (*ptr != '\0' && bytesToExchange > 0) {
            errno = EINVAL;
            perror("Not a valid number of bytes to exchange :\n");
            help();
            exit(EXIT_FAILURE);
        }
        long iteration_step = bytesToExchange;
        if (argc >= 7) {
            iteration_step = strtoll(argv[6], &ptr, 10);
            if (*ptr != '\0' && iteration_step > 0) {
                errno = EINVAL;
                perror("Not a valid number of bytes step :\n");
                help();
                exit(EXIT_FAILURE);
            }
        }
        long repeat = 0;
        if (argc >= 8) {
            repeat = strtoll(argv[7], &ptr, 10);
            if (*ptr != '\0' && repeat >= 0) {
                errno = EINVAL;
                perror("Not a valid number of iterations :\n");
                help();
                exit(EXIT_FAILURE);
            }
        }
        err = client_throughput_measure_loop(protocol, serverIP, serverPort, bytesToExchange, iteration_step,
                                             (int) repeat);
    } else if (!strcmp(argv[4], "max_connections")) {
        err = 1;// TODO
    } else if (!strcmp(argv[4], "pingpong") || !strcmp(argv[4], "pingpong_window")) {
        iterations = strtoll(argv[5], &ptr, 10);
        if (*ptr != '\0' && iterations > 0) {
            errno = EINVAL;
            perror("Not a valid number of iterations :\n");
            help();
            exit(EXIT_FAILURE);
        }
        long no_capture = 0;
        if (argc >= 7) {
            no_capture = strtoll(argv[6], &ptr, 10);
            if (*ptr != '\0' && (no_capture != 0 && no_capture != 1)) {
                errno = EINVAL;
                perror("Not a valid number of iterations :\n");
                help();
                exit(EXIT_FAILURE);
            }
        }
        int window_sending = !strcmp(argv[4], "pingpong_window");
        err = client_pingpong(protocol, serverIP, serverPort, iterations, no_capture, window_sending);
    } else {
        errno = EINVAL;
        perror("Not a valid behaviour :\n");
        help();
        exit(EXIT_FAILURE);
    }

    if (err) {
        fprintf(stderr, "The performance test failed to execute\n");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}
