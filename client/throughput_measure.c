#include <sys/types.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>

#include <openssl/bio.h>

#include "../include/main.h"
#include "../include/connect.h"

#define BUF_SIZE 4096


static int free_structures(int sfd, BIO *bio) {
    if (sfd >= 0 && close(sfd)) {
        perror("Error closing socket :\n");
        return 1;
    } else if (bio) {
        BIO_free_all(bio);
        return 0;
    }
    return 0;
}

int client_throughput_measure(int protocol, const char *serverIP, const char *serverPort, long bytesToExchange,
                              int repeat_number) {

    int sfd = -1;
    BIO *bio = NULL;
    long i;

    char *buffer = calloc(1, BUF_SIZE);
    long block_number = bytesToExchange/BUF_SIZE + (bytesToExchange % BUF_SIZE != 0);
    struct timeval tval_array[block_number+1];
    struct timeval tval_result;

    ssize_t message_length = 0;
    ssize_t sent_length = 0;
    ssize_t to_be_written = 0;

    if (protocol == MPTCPSEC) {
        sfd = mptcpsec_connect(serverIP, serverPort);
        if (sfd == -1) {
            fprintf(stderr, "Connection Failed !\n");
            free(buffer);
            return 1;
        } else if (sfd == -2) {
            errno = EINVAL;
            perror("Not a valid IP address or port number :\n");
            free(buffer);
            return 1;
        }
    } else if (protocol == TLS) {
        bio = tls_connect(serverIP, serverPort);
        if (!bio) {
            fprintf(stderr, "Connection Failed !\n");
            free(buffer);
            return 1;
        }
    } else if (protocol == MPTCP) {
        sfd = mptcp_connect(serverIP, serverPort);
        if (sfd == -1) {
            fprintf(stderr, "Connection Failed !\n");
            free(buffer);
            return 1;
        } else if (sfd == -2) {
            errno = EINVAL;
            perror("Not a valid IP address or port number :\n");
            free(buffer);
            return 1;
        }
    }

    for (i=0; i < block_number; i++) {
        message_length = 0;

        gettimeofday(&tval_array[i], NULL);

        do {
            to_be_written = BUF_SIZE - message_length;
            to_be_written = to_be_written >= 0 ? to_be_written : 0;
            if (protocol == MPTCPSEC || protocol == MPTCP) {
                sent_length = send(sfd, buffer + message_length, (size_t) to_be_written, 0);
            } else if (protocol == TLS) {
                sent_length = BIO_write(bio, buffer + message_length, (int) to_be_written);
            }
            message_length = (sent_length == -1 || sent_length == 0) ? sent_length : message_length + sent_length;
        } while (message_length > 0 && message_length < BUF_SIZE);
        if (message_length < 0) {
            perror("Error writing to socket :\n");
            free_structures(sfd, bio);
            free(buffer);
            return 1;
        }
    }

    if (free_structures(sfd, bio)) {
        free(buffer);
        return 1;
    }

    gettimeofday(&tval_array[i], NULL);

    timersub(&tval_array[block_number], &tval_array[0], &tval_result);

    printf("Time elapsed for data transfer (negotiation time not counted): %ld.%06ld\n", (long int) tval_result.tv_sec,
           (long int) tval_result.tv_usec);

    throughtput_store_measures(tval_array, (size_t) block_number + 1, 0, protocol, bytesToExchange, repeat_number);

    free(buffer);
    return 0;
}

int client_throughput_measure_loop(int protocol, const char *serverIP, const char *serverPort, long bytesToExchange,
                                   long iterationStep, int repeat) {

    int err = 0;
    char *ptr = NULL;
    int i = 0;

    char *current_server_port = malloc(strlen(serverPort) + 10);
    long current_port = strtoll(serverPort, &ptr, 10); // Should work because check before

    long transfer_size = 0;

    for (i = 0; i < repeat; i++) {
        printf("Iteration %d\n", i);
        for (transfer_size = iterationStep; transfer_size <= bytesToExchange;
             transfer_size += iterationStep, current_port += 1) {

            printf("Transfer size %ld - server port %ld\n", transfer_size, current_port);

            snprintf(current_server_port, strlen(serverPort) + 10, "%ld", current_port);

            err = client_throughput_measure(protocol, serverIP, current_server_port, transfer_size, i);
            if (err) {
                fprintf(stderr, "Transfer failed\n");
                return err;
            }

            sleep(2);
        }
    }

    return err;
}
