#include "../include/pingpong.h"


int client_pingpong(int protocol, const char *serverIP, const char *serverPort, long iterations, long no_capture,
                    int window_sending) {

    int err = 0;
    int i = 0;
    char *ptr = NULL;

    char *current_server_port = malloc(strlen(serverPort) + 10);
    long current_port = strtoll(serverPort, &ptr, 10); // Should work because check before

    for (i=0; i < iterations; i++, current_port++) {
        printf("Iteration number %d\n", i);
        snprintf(current_server_port, strlen(serverPort) + 10, "%ld", current_port);
        err = pingpong(1, protocol, serverIP, current_server_port, i, no_capture, window_sending);
    }

    return err;
}
