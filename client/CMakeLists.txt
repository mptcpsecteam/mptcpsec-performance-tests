link_libraries(ssl)
link_libraries(crypto)

set(SOURCE_FILES client.c mptcpsec_max_connections.c throughput_measure.c
        tls_max_connections.c ../include/main.h ../include/throughput_measure.h
        ../include/max_connections.h connect.c ../include/connect.h ../include/pingpong.h pingpong.c
        ../include/listen.h ../server/listen.c)
add_executable(client ${SOURCE_FILES})
