#ifndef PERF_MAIN_H
#define PERF_MAIN_H

#include <fcntl.h>
#include <string.h>

#define MPTCPSEC    0
#define TLS         1
#define MPTCP       2

#define MPTCP_SECURITY_PREFERENCE 43
#define MPTCP_ENCR_MUST 2

#define AES_GCM_TAG_LEN 16
#define TLS_RECORD_HEADER_LEN 8 + 5 // Explicit nonce + (content type, version and length)
#define MPTCPSEC_OPTION_TAG_LEN 2*AES_GCM_TAG_LEN
#define ADDITIONAL_PROTOCOL_SIZE (MPTCPSEC_OPTION_TAG_LEN > TLS_RECORD_HEADER_LEN ? \
                                  MPTCPSEC_OPTION_TAG_LEN : TLS_RECORD_HEADER_LEN)
#define ADDITIONAL_CIPHER_SIZE (AES_GCM_TAG_LEN + ADDITIONAL_PROTOCOL_SIZE)

#define MAX_PATH_SIZE 1000
#define SERVER_EXECUTABLE "server"
#define SERVER_CERTIFICATE "server.pem"
#define CAPTURE_PROGRAM "tcpdump_capture_packets.py"// "capture_packets.py"

char *execution_path;

static inline char *server_full_path(void) {

    char *full_path = malloc(MAX_PATH_SIZE);
    snprintf(full_path, MAX_PATH_SIZE, "%s/%s", execution_path, SERVER_CERTIFICATE);

    return full_path;
}

static inline char *capture_full_path(void) {

    char *full_path = malloc(MAX_PATH_SIZE);
    snprintf(full_path, MAX_PATH_SIZE, "%s/%s", execution_path, CAPTURE_PROGRAM);

    return full_path;
}

#define MAX_LINE_SIZE 300
#define MAX_FILE_NAME 255

static inline int throughtput_store_measures(struct timeval *results, size_t results_length, int server, int protocol,
                                             long bytesToExchange, int repeat_number) {

    char *file_name = malloc(MAX_FILE_NAME);
    FILE *file;
    char *line;
    size_t nbytes = 1;
    int i;

    snprintf(file_name, MAX_FILE_NAME, "%s_throughput_measure_%s_%d_%ld", (protocol == TLS) ? "tls" :
                                                                          (protocol == MPTCPSEC ? "mptcpsec" : "mptcp"),
             server ? "server" : "client", repeat_number, bytesToExchange);

    printf("File name = %s\n", file_name);

    file = fopen(file_name, "w");

    free(file_name);

    if (!file) {
        perror("Cannot create/open file");
        return 1;
    }

    line = malloc(MAX_LINE_SIZE);

    for (i = 0; i < results_length && nbytes > 0; i++) {
        snprintf(line, MAX_LINE_SIZE, "%ld.%ld\n", (long int) (results[i].tv_sec), (long int) results[i].tv_usec);
        nbytes = fwrite((void *) line, strlen(line), (size_t) 1, file);
        if (nbytes < 0) {
            perror("Cannot write into the file");
        }
    }

    free(line);

    if (nbytes < 0) {
        fclose(file);
        return 1;
    }

    return fclose(file);
}

static inline int pingpong_store_measures(struct timeval *results, size_t results_length, int client, int protocol,
                                          int iteration_number, long no_capture, int window_sending) {

    char *file_name = malloc(MAX_FILE_NAME);
    FILE *file;
    char *line;
    size_t nbytes = 1;
    int i;

    printf("Store measures ! %zu\n", results_length);

    snprintf(file_name, MAX_FILE_NAME, "data_%s_pingpong_%s_%d%s%s", (protocol == TLS) ? "tls" :
                                                                   (protocol == MPTCPSEC ? "mptcpsec" : "mptcp"),
             client ? "client" : "server", iteration_number, no_capture ? "_no_capture" : "",
             window_sending ? "_window" : "");

    file = fopen(file_name, "w");

    free(file_name);

    if (!file) {
        perror("Cannot create/open file");
        return 1;
    }

    line = malloc(MAX_LINE_SIZE);

    for (i = 0; i < results_length && nbytes > 0; i++) {
        snprintf(line, MAX_LINE_SIZE, "%ld.%ld\n", (long int) (results[i].tv_sec), (long int) results[i].tv_usec);
        nbytes = fwrite((void *) line, strlen(line), (size_t) 1, file);
        if (nbytes < 0) {
            perror("Cannot write into the file");
        }
    }

    free(line);

    if (nbytes < 0) {
        fclose(file);
        return 1;
    }

    return fclose(file);
}

#endif //PERF_MAIN_H
