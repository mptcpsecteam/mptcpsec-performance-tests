#ifndef PERF_THROUGHPUT_MEASURE_H
#define PERF_THROUGHPUT_MEASURE_H

int client_throughput_measure(int protocol, const char *serverIP, const char *serverPort, long bytesToExchange,
                              int repeat);
int server_throughput_measure(int protocol, const char *serverIP, const char *serverPort, long bytesToExchange,
                              int repeat);
int client_throughput_measure_loop(int protocol, const char *serverIP, const char *serverPort, long bytesToExchange,
                                   long iterationStep, int repeat);
int server_throughput_measure_loop(int protocol, const char *serverIP, const char *serverPort, long bytesToExchange,
                                   long iterationStep, int repeat);

#endif //PERF_THROUGHPUT_MEASURE_H
