#ifndef PERF_PINGPONG_H
#define PERF_PINGPONG_H

#include <sys/types.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/tcp.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <wait.h>

#include "main.h"
#include "connect.h"
#include "listen.h"

#define MSS_INCREMENT 100
#define MAX_MSS 10000

#define WINDOW_INCREMENT 1000
#define MAX_WINDOW 27000

int client_pingpong(int protocol, const char *serverIP, const char *serverPort, long iterations, long no_capture,
                    int window_sending);
int server_pingpong(int protocol, const char *serverIP, const char *serverPort, long iterations, long no_capture,
                    int window_sending);

static inline int free_structures(int sfd, BIO *bio, int listen_only) {
    if (sfd >= 0 && close(sfd)) {
        return 1;
    } else if (bio) {
        SSL *ssl;
        BIO_get_ssl(bio, &ssl);
        if (!listen_only) {
            int err = SSL_shutdown(ssl);
            if (err) {
                ERR_print_errors_fp(stderr);
            }
            sfd = (int) BIO_get_fd(bio, &sfd);
            if (sfd > 0) {
                close(sfd);
            }
            SSL_free(ssl);
        } else {
            BIO_free_all(bio);
        }
        return 0;
    }
    return 0;
}

static inline int get_mss(int sfd, BIO *bio, int protocol) {

    int mss = 0;
    socklen_t mms_len = sizeof(mss);

    if (((protocol == MPTCPSEC || protocol == MPTCP) && getsockopt(sfd, IPPROTO_TCP, TCP_MAXSEG, &mss, &mms_len)
         || (protocol == TLS && getsockopt((int) BIO_get_fd(bio, NULL), IPPROTO_TCP, TCP_MAXSEG, &mss, &mms_len)))) {
        perror("Impossible to get MSS value");
        return -1;
    }
    /* We remove size that will be needed for the authentication tag or the TLS record header size */
    mss = mss - ADDITIONAL_CIPHER_SIZE;

    return mss;
}

static inline int get_window(int sfd, BIO*bio, int protocol) {

    int window = 0;
    socklen_t window_len = sizeof(window);
    if (((protocol == MPTCPSEC || protocol == MPTCP) && getsockopt(sfd, SOL_SOCKET, SO_SNDBUF, &window, &window_len)
         || (protocol == TLS && getsockopt((int) BIO_get_fd(bio, NULL), SOL_SOCKET, SO_SNDBUF, &window, &window_len)))) {
        perror("Impossible to get window value");
        return -1;
    }

    int mss = 0;
    socklen_t mms_len = sizeof(mss);
    if (((protocol == MPTCPSEC || protocol == MPTCP) && getsockopt(sfd, IPPROTO_TCP, TCP_MAXSEG, &mss, &mms_len)
         || (protocol == TLS && getsockopt((int) BIO_get_fd(bio, NULL), IPPROTO_TCP, TCP_MAXSEG, &mss, &mms_len)))) {
        perror("Impossible to get MSS value");
        return -1;
    }

    window = MAX_WINDOW;

    /* We remove size that will be needed for the authentication tag or the TLS record header size */
    window = window - (window / mss) * (ADDITIONAL_CIPHER_SIZE);

    return window;
}

static inline int send_segment(int buff_size, int sfd, BIO *bio, char *buffer, int protocol) {

    ssize_t sent_length = 0;
    ssize_t to_be_written = buff_size;

    if (protocol == MPTCPSEC || protocol == MPTCP) {
        sent_length = send(sfd, buffer, (size_t) to_be_written, 0);
    } else {
        sent_length = BIO_write(bio, buffer, (int) to_be_written);
    }
    if (sent_length < 0) {
        perror("Error writing to socket :\n");
        free_structures(sfd, bio, 0);
        free(buffer);
        return 1;
    } else if (sent_length < buff_size) {
        fprintf(stderr, "Not all the data available was sent : buffer size = %zd, data sent = %zd\n", to_be_written,
                sent_length);
        return -1;
    }

    return 0;
}

static inline int recv_segment(int buff_size, int sfd, BIO *bio, char *buffer, int protocol) {

    ssize_t recv_length = 0;
    ssize_t to_be_read = buff_size;

    if (protocol == MPTCPSEC || protocol == MPTCP) {
        recv_length = recv(sfd, buffer, (size_t) to_be_read, 0);
    } else {
        recv_length = BIO_read(bio, buffer, (int) to_be_read);
    }
    if (recv_length < 0) {
        perror("Error writing to socket :\n");
        free_structures(sfd, bio, 0);
        free(buffer);
        return 1;
    } else if (recv_length < buff_size) {
        fprintf(stderr, "Not all the data available was received : buffer size = %zd, data received = %zd\n", to_be_read,
                recv_length);
        return -1;
    }

    return 0;
}

static inline int send_data(int buff_size, int sfd, BIO *bio, char *buffer, int protocol) {

    ssize_t sent_length = 0;
    ssize_t current_sending_result = 0;
    ssize_t to_be_written;

    for (sent_length = 0; sent_length < buff_size; sent_length += current_sending_result) {
        to_be_written = buff_size - sent_length;

        if (protocol == MPTCPSEC || protocol == MPTCP) {
            current_sending_result = send(sfd, buffer + sent_length, (size_t) to_be_written, 0);
        } else {
            current_sending_result = BIO_write(bio, buffer + sent_length, (int) to_be_written);
        }
        if (current_sending_result < 0) {
            perror("Error writing to socket :\n");
            free_structures(sfd, bio, 0);
            free(buffer);
            return 1;
        }
    }

    return 0;
}

static inline int recv_data(int buff_size, int sfd, BIO *bio, char *buffer, int protocol) {

    ssize_t recv_length = 0;
    ssize_t to_be_read;
    ssize_t current_receiving_result;

    for (recv_length = 0; recv_length < buff_size; recv_length += current_receiving_result) {
        to_be_read = buff_size - recv_length;

        if (protocol == MPTCPSEC || protocol == MPTCP) {
            current_receiving_result = recv(sfd, buffer + recv_length, (size_t) to_be_read, 0);
        } else {
            current_receiving_result = BIO_read(bio, buffer + recv_length, (int) to_be_read);
        }
        if (current_receiving_result < 0) {
            perror("Error writing to socket :\n");
            free_structures(sfd, bio, 0);
            free(buffer);
            return 1;
        }
    }

    return 0;
}

static int start_capture(int protocol, int iteration_number, const char *serverPort, int buff_size, int window_sending) {
    char *protocol_str = (protocol == TLS) ? "tls" :
                         (protocol == MPTCPSEC ? "mptcpsec" : "mptcp");
    char *iteration_str = malloc(100);
    snprintf(iteration_str, 100, "%d", iteration_number);
    char *serverPort_copy = malloc(strlen(serverPort));
    snprintf(serverPort_copy, 100, "%s", serverPort);
    char *send_size = malloc(100);
    snprintf(send_size, 100, "%d", buff_size);
    char *capture_path = capture_full_path();
    char *capture_arguments[7] = {capture_path, serverPort_copy, protocol_str, iteration_str, send_size,
                                  window_sending ? "True" : "False", NULL};

    fflush(stdout);
    fflush(stderr);

    int pid = fork();
    if (pid < 0) { /* Error */
        perror("fork() failed : \n");
        return -1;
    } else if (pid == 0) { /* Child process */
        int err = execvp(capture_path, capture_arguments);
        perror("execvp() failed");
        fprintf(stderr, "Error code %d\n", err);
        return err;
    }

    return pid;
}

static int stop_capture(int pid) {

    if (kill(pid, SIGINT)) {
        perror("Fail to close the process :");
        return 1;
    }
    return 0;
}

static int new_connection(int client, int protocol, const char *serverIP, const char *serverPort, int iteration_number,
                          long noCapture, int *sfd, BIO **bio, int *listen_sfd, BIO **listening_bio, long loop_iter,
                          char *buffer, struct timeval *tval_array, int window_sending) {

    if (client) {
        if (protocol == MPTCPSEC) {
            *sfd = mptcpsec_connect(serverIP, serverPort);
            if (*sfd == -1) {
                fprintf(stderr, "Connection Failed !\n");
                free(buffer);
                if (loop_iter != 0)
                    pingpong_store_measures(tval_array, (size_t) loop_iter, client, protocol, iteration_number,
                                            noCapture, window_sending);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                if (loop_iter != 0)
                    pingpong_store_measures(tval_array, (size_t) loop_iter, client, protocol, iteration_number,
                                            noCapture, window_sending);
                return 1;
            }
        } else if (protocol == MPTCP) {
            *sfd = mptcp_connect(serverIP, serverPort);
            if (*sfd == -1) {
                fprintf(stderr, "Connection Failed !\n");
                free(buffer);
                if (loop_iter != 0)
                    pingpong_store_measures(tval_array, (size_t) loop_iter, client, protocol, iteration_number,
                                            noCapture, window_sending);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                if (loop_iter != 0)
                    pingpong_store_measures(tval_array, (size_t) loop_iter, client, protocol, iteration_number,
                                            noCapture, window_sending);
                return 1;
            }
        } else if (protocol == TLS) {
            *bio = tls_connect(serverIP, serverPort);
            if (!*bio) {
                fprintf(stderr, "Connection Failed !\n");
                free(buffer);
                if (loop_iter != 0)
                    pingpong_store_measures(tval_array, (size_t) loop_iter, client, protocol, iteration_number,
                                            noCapture, window_sending);
                return 1;
            }
        }
    } else if (*listen_sfd < 0 && !*listening_bio) { // Server setup
        if (protocol == MPTCPSEC) {
            *sfd = mptcpsec_listen(serverIP, serverPort, listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Listen Failed !\n");
                free(buffer);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                return 1;
            }
        } else if (protocol == MPTCP) {
            *sfd = mptcp_listen(serverIP, serverPort, listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Listen Failed !\n");
                free(buffer);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                return 1;
            }
        } else if (protocol == TLS) {
            *bio = tls_listen(serverIP, serverPort, listening_bio);
            if (!*bio) {
                fprintf(stderr, "Listen Failed !\n");
                free(buffer);
                return 1;
            }
        }
    } else {
        if (protocol == MPTCPSEC) {
            *sfd = mptcpsec_accept(*listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Accept Failed !\n");
                free_structures(*listen_sfd, *listening_bio, 1);
                free(buffer);
                return 1;
            }
        } else if (protocol == MPTCP) {
            *sfd = mptcp_accept(*listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Accept Failed !\n");
                free_structures(*listen_sfd, *listening_bio, 1);
                free(buffer);
                return 1;
            }
        } else if (protocol == TLS) {
            *bio = tls_accept(*listening_bio);
            if (!*bio) {
                fprintf(stderr, "Accept Failed !\n");
                free_structures(*listen_sfd, *listening_bio, 1);
                free(buffer);
                return 1;
            }
        }
    }

    return 0;
}

static int pingpong_behaviour(int client, int protocol, int sfd, BIO *bio, int listen_sfd, BIO *listening_bio,
                              long loop_iter, char *buffer, int buff_size, struct timeval *tval_array,
                              int window_sending) {

    if (client) {
        gettimeofday(&tval_array[loop_iter], NULL); // Take time before sending

        int err = 0;
        if (window_sending) {
            err = send_data(buff_size, sfd, bio, buffer, protocol);
        } else {
            err = send_segment(buff_size, sfd, bio, buffer, protocol);
        }
        if (err > 0) {
            fprintf(stderr, "Sending %s failed !\n", window_sending ? "data" : "segment");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        } else if (err < 0) {
            fprintf(stderr, "Sending %s was not complete !\n", window_sending ? "data" : "segment");
            free_structures(sfd, bio, 0);
            return -1;
        }

        gettimeofday(&tval_array[loop_iter + 1], NULL); // Take time before receiving

        if (window_sending) {
            err = recv_data(buff_size, sfd, bio, buffer, protocol);
        } else {
            err = recv_segment(buff_size, sfd, bio, buffer, protocol);
        }
        if (err > 0) {
            fprintf(stderr, "Receiving %s failed !\n", window_sending ? "data" : "segment");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        } else if (err < 0) {
            free_structures(sfd, bio, 0);
            fprintf(stderr, "Receiving %s was not complete !\n", window_sending ? "data" : "segment");
            return -1;
        }

        gettimeofday(&tval_array[loop_iter + 2], NULL); // Take time after operations
    } else {
        gettimeofday(&tval_array[loop_iter], NULL); // Take time before receiving

        int err;
        if (window_sending) {
            err = recv_data(buff_size, sfd, bio, buffer, protocol);
        } else {
            err = recv_segment(buff_size, sfd, bio, buffer, protocol);
        }
        if (err > 0) {
            fprintf(stderr, "Receiving segment failed !\n");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        } else if (err < 0) {
            free_structures(sfd, bio, 0);
            fprintf(stderr, "Receiving segment was not complete !\n");
            return -1;
        }

        gettimeofday(&tval_array[loop_iter + 1], NULL); // Take time before sending

        if (window_sending) {
            err = send_data(buff_size, sfd, bio, buffer, protocol);
        } else {
            err = send_segment(buff_size, sfd, bio, buffer, protocol);
        }
        if (err > 0) {
            fprintf(stderr, "Sending segment failed !\n");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        } else if (err < 0) {
            fprintf(stderr, "Sending segment was not complete !\n");
            free_structures(sfd, bio, 0);
            return -1;
        }

        gettimeofday(&tval_array[loop_iter + 2], NULL); // Take time after operations
    }

    return 0;
}

static int pingpong(int client, int protocol, const char *serverIP, const char *serverPort, int iteration_number,
                    long noCapture, int window_sending) {

    int sfd = -1;
    int listen_sfd = -1;
    int pid = -1;
    BIO *bio = NULL;
    BIO *listening_bio = NULL;

    int increment = window_sending ? WINDOW_INCREMENT : MSS_INCREMENT;
    int max_limit = window_sending ? MAX_WINDOW : MAX_MSS;
    char *buffer = calloc(1, (size_t) max_limit);

    int blocks = max_limit / increment + (max_limit % increment != 0);
    struct timeval tval_array[blocks*3];
    struct timeval tval_result;

    long i = 0;
    int buff_size = increment;
    do {

        sleep(2); // Sleep in order to prevent previous connections from interfering inside the capture

        if (pid > 0 && stop_capture(pid)) {
            fprintf(stderr, "Stopping capture has failed\n");
            return 1;
        }

        sleep(1);

        /* New Connection */

        if (new_connection(client, protocol, serverIP, serverPort, iteration_number, noCapture, &sfd, &bio, &listen_sfd,
                           &listening_bio, i, buffer, tval_array, window_sending)) {
            fprintf(stderr, "Cannot establish/receive the connection\n");
            return 1;
        }

        /* Get new max limit */
        if (window_sending) {
            max_limit = get_window(sfd, bio, protocol);
        } else {
            max_limit = get_mss(sfd, bio, protocol);
        }
        if (max_limit < 0) {
            fprintf(stderr, "Getting new max limit failed !\n");
            free_structures(listen_sfd, listening_bio, 1);
            free_structures(sfd, bio, 0);
            free(buffer);
            return 1;
        }

        /* Start capture if we are on server side */
        if (!client && !noCapture) {
            pid = start_capture(protocol, iteration_number, serverPort, buff_size, window_sending);
            if (pid < 0) {
                fprintf(stderr, "Error launching the capture\n");
                return 1;
            }
        }

        sleep(1); // To be sure that capture is launched and that crypto data structures are initialized for MPTCPsec

        int err = pingpong_behaviour(client, protocol, sfd, bio, listen_sfd, listening_bio, i, buffer, buff_size,
                                     tval_array, window_sending);
        if (err > 0) {
            return 1;
        } else if (err < 0) {
            break;
        }

        /* Close connection */

        if (free_structures(sfd, bio, 0)) {
            perror("Error closing connection !\n");
            free_structures(listen_sfd, listening_bio, 1);
            free(buffer);
            return 1;
        }

        timersub(&tval_array[i+2], &tval_array[i], &tval_result);

        printf("Time elapsed for data transfer (negotiation time and closing time not counted): %ld.%06ld\n",
               (long int) tval_result.tv_sec, (long int) tval_result.tv_usec);

        /* Variable updates */

        buff_size += increment;
        i += 3;

    } while (buff_size <= max_limit &&
            (window_sending && buff_size <= MAX_WINDOW || !window_sending && buff_size <= MAX_MSS));

    if (free_structures(listen_sfd, listening_bio, 1)) {
        fprintf(stderr, "Problem freeing the listening socket\n");
        free(buffer);
        return 1;
    }

    pingpong_store_measures(tval_array, (size_t) i, client, protocol, iteration_number, noCapture, window_sending);

    free(buffer);

    if (pid > 0 && stop_capture(pid)) {
        fprintf(stderr, "Stopping capture has failed\n");
        return 1;
    }

    sleep(1);

    return 0;
}

#endif //PERF_PINGPONG_H
