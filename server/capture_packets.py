#!/usr/bin/env python

from scapy.all import *

import sys
import os


if len(sys.argv) < 5:
    print("Not enough arguments")
else:
    print(str(sys.argv))

portNumber = sys.argv[1]
protocol = sys.argv[2]
iteration = sys.argv[3]
sent_size = int(sys.argv[4])

additional_payload = 0
if protocol == "mptcpsec":
    additional_payload = 3 * 16  # Option tag + data tag
elif protocol == "tls":
    additional_payload = 5 + 8 + 16  # (content type, version and length) + explicit nonce + authentication tag
cipher_size = sent_size + additional_payload

ping_seen = False

FIN_MASK = 0x01
RST_MASK = 0x04

pkt_list = []


def keep_going(new_pkt):

    global ping_seen
    global pkt_list

    if TCP in new_pkt and (new_pkt[TCP].dport == int(portNumber) or new_pkt[TCP].sport == int(portNumber)):

        pkt_list.append(new_pkt)

        payload_size = len(new_pkt[TCP].payload)

        pong_seen = ping_seen and payload_size == cipher_size
        ping_seen = ping_seen or payload_size == cipher_size

        return not pong_seen and new_pkt[TCP].flags & FIN_MASK == 0 and new_pkt[TCP].flags & RST_MASK == 0

    return True

sniff(filter="tcp and port " + portNumber, store=0, stop_filter=lambda new_pkt: not keep_going(new_pkt))
capture_file = protocol + "_processing_measure_server_" + iteration + "_" + str(sent_size) + ".pcap"
wrpcap(capture_file, pkt_list)

os.chmod(capture_file, 0o666)
