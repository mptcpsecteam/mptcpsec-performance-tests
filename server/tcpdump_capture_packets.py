#!/usr/bin/env python3

import sys
import os
import subprocess
import signal
import shutil


if len(sys.argv) < 5:
    print("Not enough arguments")
else:
    print(str(sys.argv))

portNumber = sys.argv[1]
protocol = sys.argv[2]
iteration = sys.argv[3]
sent_size = sys.argv[4]

if len(sys.argv) >= 6:
    window = "_window" if sys.argv[5] == "True" else ""
else:
    window = ""


# Initialize ramdisk if needed
ramdisk = "/virtuelram"
init_file = "init"
os.makedirs(ramdisk, mode=0o777, exist_ok=True)
if not os.path.exists(os.path.join(ramdisk, init_file)):  # Ramdisk not mounted

    process = subprocess.Popen(["sudo", "mount", "-t", "tmpfs", "-o", "size=50M", "tmpfs", ramdisk],
                               universal_newlines=True)
    process.wait()
    if process.returncode != 0:
        print("Initialize ramdisk has failed !")
        sys.exit(1)

    open(os.path.join(ramdisk, init_file), "w").close()


# Launch capture
capture_file = protocol + "_processing_measure_server_" + iteration + "_" + str(sent_size) + window + ".pcap"
cmd = ["tcpdump", "-w", os.path.join(ramdisk, capture_file), "-i", "any", "tcp", "and", "port", portNumber]
process = subprocess.Popen(cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def signal_handler(sig, frame):
    print('tcpdump is going to be closed')
    process.terminate()
    process.communicate()
    process.wait()
    shutil.move(os.path.join(ramdisk, capture_file), capture_file)
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.pause()
