#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"

#include "../include/main.h"

/**
 * Code Inspired from the man page of getaddrinfo(3)
 * (see http://linux.die.net/man/3/getaddrinfo)
 */
int mptcpsec_listen(const char *serverIP, const char *serverPort, int *listening_sfd) {

    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd = -1;
    int newsfd;
    int s;
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len = sizeof(peer_addr);

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* TCP socket */
    hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
    hints.ai_protocol = 0;          /* Any protocol */
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo(serverIP, serverPort, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -2;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                     rp->ai_protocol);
        if (sfd == -1)
            continue;

        int val = 1;
        socklen_t sock_opt_va_len = sizeof(val);
        if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &val, sock_opt_va_len)) {
            fprintf(stderr, "Could not reuse !\n");
            close(sfd);
            continue;
        }

        int sock_opt_val = MPTCP_ENCR_MUST;
        sock_opt_va_len = sizeof(sock_opt_val);
        if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0
            && !setsockopt(sfd, IPPROTO_TCP, MPTCP_SECURITY_PREFERENCE,
                           &sock_opt_val, sock_opt_va_len))
            break; /* Success */

        close(sfd);
    }

    if (rp == NULL) { /* No address succeeded */
        fprintf(stderr, "Could not bind\n");
        return -1;
    }

    freeaddrinfo(result); /* No longer needed */

    listen(sfd, 5);

    newsfd = accept(sfd, (struct sockaddr *) &peer_addr, &peer_addr_len);

    if (listening_sfd) {
        *listening_sfd = sfd;
    } else {
        close(sfd);
    }

    return newsfd;
}

/**
 * Code inspired from the man page of bio_new_ssl(3)
 * (see http://linux.die.net/man/3/bio_new_ssl)
 */
BIO *tls_listen(const char *serverIP, const char *serverPort, BIO **listen_bio) {

    BIO *sbio, *acpt;
    SSL_CTX *ctx;
    SSL *ssl;

    char *serverPortCopy = malloc(strlen(serverPort));
    char *certificate = server_full_path();

    strncpy(serverPortCopy, serverPort, strlen(serverPort));

    ctx = SSL_CTX_new(TLSv1_2_server_method());

    if (!SSL_CTX_use_certificate_file(ctx, certificate, SSL_FILETYPE_PEM)
        || !SSL_CTX_use_PrivateKey_file(ctx, certificate, SSL_FILETYPE_PEM)
        || !SSL_CTX_check_private_key(ctx)) {

        fprintf(stderr, "Error setting up SSL_CTX\n");
        ERR_print_errors_fp(stderr);
        free(serverPortCopy);
        free(certificate);
        return NULL;
    }

    /* New SSL BIO setup as server */
    sbio = BIO_new_ssl(ctx, 0);

    BIO_get_ssl(sbio, &ssl);

    if(!ssl) {
        fprintf(stderr, "Can't locate SSL pointer\n");
        free(serverPortCopy);
        free(certificate);
        return NULL;
    }

    /* Don't want any retries */
    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

    /* Force the cipher to AES128-GCM in order to be able to compare it
     * to MPTCPsec that only support this algorithm for now
     */
    SSL_set_cipher_list(ssl, "AES128-GCM-SHA256:DH-RSA-AES128-GCM-SHA256:DH-DSS-AES128-GCM-SHA256:"
            "DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:"
            "ECDHE-ECDSA-AES128-GCM-SHA256:ADH-AES128-GCM-SHA256");

    acpt = BIO_new_accept(serverPortCopy);
    BIO_set_bind_mode(acpt, BIO_BIND_REUSEADDR_IF_UNUSED);

    /* By doing this when a new connection is established
     * we automatically have sbio inserted into it. The
     * BIO chain is now 'swallowed' by the accept BIO and
     * will be freed when the accept BIO is freed.
     */

    BIO_set_accept_bios(acpt, sbio);

    /* Setup accept BIO */
    if(BIO_do_accept(acpt) <= 0) {
        fprintf(stderr, "Error setting up accept BIO\n");
        ERR_print_errors_fp(stderr);
        free(serverPortCopy);
        free(certificate);
        return NULL;
    }

    /* Now wait for incoming connection */
    if(BIO_do_accept(acpt) <= 0) {
        fprintf(stderr, "Error in accepting a connection\n");
        ERR_print_errors_fp(stderr);
        free(serverPortCopy);
        free(certificate);
        return NULL;
    }

    /* We only want one connection so remove and free
     * accept BIO
     */
    sbio = BIO_pop(acpt);
    if (listen_bio) {
        *listen_bio = acpt;
    } else {
        BIO_free_all(acpt);
    }
    free(serverPortCopy);
    free(certificate);

    if(BIO_do_handshake(sbio) <= 0) {
        fprintf(stderr, "Error in SSL handshake\n");
        ERR_print_errors_fp(stderr);
        return NULL;
    }

    return sbio;
}

int mptcp_listen(const char *serverIP, const char *serverPort, int *listening_sfd) {

    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd = -1;
    int newsfd;
    int s;
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len = sizeof(peer_addr);

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* TCP socket */
    hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
    hints.ai_protocol = 0;          /* Any protocol */
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo(serverIP, serverPort, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -2;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                     rp->ai_protocol);
        if (sfd == -1)
            continue;

        int val = 1;
        socklen_t sock_opt_va_len = sizeof(val);
        if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &val, sock_opt_va_len)) {
            fprintf(stderr, "Could not reuse !\n");
            close(sfd);
            continue;
        }

        if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
            break; /* Success */

        close(sfd);
    }

    if (rp == NULL) { /* No address succeeded */
        fprintf(stderr, "Could not bind\n");
        return -1;
    }

    freeaddrinfo(result); /* No longer needed */

    listen(sfd, 5);

    newsfd = accept(sfd, (struct sockaddr *) &peer_addr, &peer_addr_len);

    if (listening_sfd) {
        *listening_sfd = sfd;
    } else {
        close(sfd);
    }

    return newsfd;
}

int mptcpsec_accept(int sfd) {

    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len = sizeof(peer_addr);

    return accept(sfd, (struct sockaddr *) &peer_addr, &peer_addr_len);
}

BIO *tls_accept(BIO *bio) {

    /* Now wait for incoming connection */
    if(BIO_do_accept(bio) <= 0) {
        fprintf(stderr, "Error in accepting a connection\n");
        ERR_print_errors_fp(stderr);
        return NULL;
    }

    BIO *sbio = BIO_pop(bio);

    if(BIO_do_handshake(sbio) <= 0) {
        fprintf(stderr, "Error in SSL handshake\n");
        ERR_print_errors_fp(stderr);
        return NULL;
    }

    return sbio;
}

int mptcp_accept(int sfd) {

    return mptcpsec_accept(sfd);
}
